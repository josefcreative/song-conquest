# README #

Song Conquest UI

### AngularJS / Gulp App ###

* This app is running the latest version of AngularJS 1.6**.
* Built with Gulp.
* Uses Bootstrp 4 (beta) & Sass with Breakpoint.

### How do I get set up? ###

* Run 'npm install'.
* run 'Gulp' in the terminal to build the js/scss.
* Deployment instructions: Just drop the ~/app dir in your project.

### Who do I talk to? ###

* Repo owner or admin - Joe
