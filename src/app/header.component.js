app.controller('headerController', [function() {
    var self = this;
}])
.directive('appHeader', function() {
    return {
        templateUrl: 'views/header.component.html',
        restrict: 'E',
        replace: true,
        scope: {
            show: "=menu"
        },
        link: function(scope, elem, attrs) {
            scope.showMenu = function() {
                scope.show=!scope.show;
            }
        }

    }
})