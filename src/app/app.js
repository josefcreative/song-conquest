app = angular.module('songConquestApp', [])
    .controller('appController', [function() {

        self = this;
        self.showMenu = false;
    }])
    .directive('uiApp', function() {
        return {
            templateUrl: 'header.component.html'
        }
    });
    