app.controller('sideMenuController', [function() {
        var self = this;

}])
.directive('appSideMenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/sideMenu.component.html',
        replace: true,
        scope: {
            show: '=menu'
        },
        link : function(scope, elem, attrs) {

            scope.toggleSideMenu = function() {
                scope.show=!scope.show;
            }
        }
    }
});