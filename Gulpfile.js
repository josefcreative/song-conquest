var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var jsFiles = 'src/js/**/*.js';
var jsDest = 'app/js';

gulp.task('serve', ['scripts'], function() {
    browserSync.init({
        server: "./app"
    });

    gulp.watch("src/**/*.js", ['scripts']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
    gulp.watch("app/views/*.html").on('change', browserSync.reload);
});

gulp.task('scripts', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        'src/tether.min.js',
        './node_modules/angular/angular.js',
        './node_modules/bootstrap/dist/js/bootstrap.js',
        'src/app/app.js',
        'src/app/header.component.js',
        'src/app/sideMenu.component.js',
        'src/app/recentlyEnded.component.js',
        'src/app/topContestants.component.js',
        'src/app/topAces.component.js',
        'src/app/queuedPlayers.component.js',
        'src/app/topGamblers.component.js',
    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
        .pipe(browserSync.stream());
})


// // Watch Task
gulp.task('default', ['serve']);
